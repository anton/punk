

## Purpose

- The good news: the response composer in Zendesk Workspace [supports editing tables inline](https://support.zendesk.com/hc/en-us/articles/4408831849882-Composing-messages-in-the-Zendesk-Agent-Workspace#topic_zcl_b5p_r5b)!
- The bad news: the composer does not contain a way to _create_ new tables; tables need to be copied from another application (such as Numbers or Google Sheets) before they can be included in a response.

This script:
- Registers a shortcut in your Userscript Manager's menu
- Clicking this shortcut will set your clipboard to a simple, 2x2 HTML table element (**please note this will clear your existing clipboard**)

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_table_shortcut/script.user.js

If you do not have a userscript manager installed (such as [TamperMonkey](https://www.tampermonkey.net/) or [ViolentMonkey](https://violentmonkey.github.io/)) the script will be displayed but you will not be prompted to install it.

## Permissions

| Permission | Needed For | 
| --- | --- |
| `GM.registerMenuCommand` | Used to create the menu shortcut |
| `GM.getValue` | Planned for setting configuration in a future version |
| `GM.setValue` | Planned for setting configuration in a future version |

## Changelog

- `0.1`
    - Adds the shortcut to the Userscript manager toolbar menu


