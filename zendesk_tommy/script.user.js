// ==UserScript==
// @name          Zendesk Tommy
// @version       1.4
// @author        Rene Verschoor
// @description   Zendesk: set form fields
// @match         https://gitlab.zendesk.com/*
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_tommy
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_tommy/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_tommy/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_tommy/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function () {

  const HOTKEY_SM_PREMIUM = buildHotkey('ctrl', 'sp');
  const HOTKEY_SM_ULTIMATE = buildHotkey('ctrl', 'su');

  const HOTKEY_CLOUD_PREMIUM = buildHotkey('ctrl', 'cp');
  const HOTKEY_CLOUD_ULTIMATE = buildHotkey('ctrl', 'cu');

  const HOTKEY_CLOUD_EXEMPTION_PREMIUM = buildHotkey('ctrl', 'cep');
  const HOTKEY_CLOUD_EXEMPTION_ULTIMATE = buildHotkey('ctrl', 'ceu');

  const HOTKEY_SUBSCRIPTION_EXTENSION_SM_STARTER = buildHotkey('ctrl', 'sess');
  const HOTKEY_SUBSCRIPTION_EXTENSION_SM_PREMIUM = buildHotkey('ctrl', 'sesp');
  const HOTKEY_SUBSCRIPTION_EXTENSION_SM_ULTIMATE = buildHotkey('ctrl', 'sesu');

  const HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_STARTER = buildHotkey('ctrl', 'secs');
  const HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_PREMIUM = buildHotkey('ctrl', 'secp');
  const HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_ULTIMATE = buildHotkey('ctrl', 'secu');

  const HOTKEY_TRIAL_EXTENSION_SM = buildHotkey('ctrl', 'tes');
  const HOTKEY_TRIAL_EXTENSION_CLOUD = buildHotkey('ctrl', 'tec');

  const DELAY = 100;

  function delay(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  const plans = {
    'sm-starter': 'Starter Customer',
    'sm-premium': 'Premium customer',
    'sm-ultimate': 'Ultimate customer',
    'cloud-starter': 'Bronze customer',
    'cloud-premium': 'Silver/SaaS Premium customer',
    'cloud-ultimate': 'Gold/SaaS Ultimate customer',
    'sa-trial': 'Sales Assisted Trial'
  }

  function planToItemText(plan) {
    return plans[plan];
  }

  function buildHotkey(modifier, chars) {
    let hotkey = '';
    for (let char of chars) {
      hotkey += `${modifier}-${char} `;
    }
    return hotkey.trim();
  }

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_SM_PREMIUM, () => {
      actionSMPlan('sm-premium');
    });
    VM.shortcut.register(HOTKEY_SM_ULTIMATE, () => {
      actionSMPlan('sm-ultimate');
    });

    VM.shortcut.register(HOTKEY_CLOUD_PREMIUM, () => {
      actionCloudPlan('cloud-premium');
    });
    VM.shortcut.register(HOTKEY_CLOUD_ULTIMATE, () => {
      actionCloudPlan('cloud-ultimate');
    });

    VM.shortcut.register(HOTKEY_CLOUD_EXEMPTION_PREMIUM, () => {
      actionCloudExemption('sm-premium');
    });
    VM.shortcut.register(HOTKEY_CLOUD_EXEMPTION_ULTIMATE, () => {
      actionCloudExemption('sm-ultimate');
    });

    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_SM_STARTER, () => {
      actionSubscriptionExtensionSM('sm-starter');
    });
    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_SM_PREMIUM, () => {
      actionSubscriptionExtensionSM('sm-premium');
    });
    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_SM_ULTIMATE, () => {
      actionSubscriptionExtensionSM('sm-ultimate');
    });

    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_STARTER, () => {
      actionSubscriptionExtensionCloud('cloud-starter');
    });
    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_PREMIUM, () => {
      actionSubscriptionExtensionCloud('cloud-premium');
    });
    VM.shortcut.register(HOTKEY_SUBSCRIPTION_EXTENSION_CLOUD_ULTIMATE, () => {
      actionSubscriptionExtensionCloud('cloud-ultimate');
    });

    VM.shortcut.register(HOTKEY_TRIAL_EXTENSION_SM, () => {
      actionTrialExtensionSM();
    });
    VM.shortcut.register(HOTKEY_TRIAL_EXTENSION_CLOUD, () => {
      actionTrialExtensionCloud();
    });
  }

  async function actionSMPlan(plan) {
    await setProductType('Self-Managed');
    await setSMPlan(plan);
  }

  async function actionCloudPlan(plan) {
    await setProductType('GitLab.com (SaaS)');
    await setCloudPlan(plan);
  }
  async function actionCloudExemption(plan) {
    await setTransactionIssueType('Cloud Licensing (SM only)');
    await setLicenseTroubleshooting('Other');
    await setProductType('Self-Managed');
    await setSMPlan(plan);
  }

  async function actionSubscriptionExtensionSM(plan) {
    await setTransactionIssueType('Subscription Extension');
    await setProductType('Self-Managed');
    await setSMPlan(plan);
  }

  async function actionSubscriptionExtensionCloud(plan) {
    await setTransactionIssueType('Subscription Extension');
    await setProductType('GitLab.com (SaaS)');
    await setCloudPlan(plan);
  }

  async function actionTrialExtensionSM() {
    await setTransactionIssueType('Trial - related inquiries');
    await setTrialRelatedInquiries('Trial extension');
    await setProductType('Self-Managed');
    await setSMPlan('sa-trial');
  }

  async function actionTrialExtensionCloud() {
    await setTransactionIssueType('Trial - related inquiries');
    await setTrialRelatedInquiries('Trial extension');
    await setProductType('GitLab.com (SaaS)');
    await setCloudPlan('sa-trial');
  }

  async function setTransactionIssueType(type) {
    await selectItem('.custom_field_360020421853', type);
  }

  async function setLicenseTroubleshooting(type) {
    await selectItem('.custom_field_360005878859', type);
  }

  async function setProductType(type) {
    await selectItem('.custom_field_360012882099', type);
  }

  async function setSMPlan(plan) {
    const itemText = planToItemText(plan);
    await selectItem('.custom_field_360005182220', itemText);
  }

  async function setCloudPlan(plan) {
    const itemText = planToItemText(plan);
    await selectItem('.custom_field_360005241679', itemText);
  }

  async function setTrialRelatedInquiries(type) {
    await selectItem('.custom_field_360005878959', type);
  }

  async function selectItem(field, value) {
    let ticket= getActiveTicket();
    ticket.querySelector(`${field} [data-garden-id="dropdowns.faux_input"]`).click();
    await delay(DELAY);
    const items = document.querySelectorAll(`${field} li`);
    const index = [...items].findIndex(i => i.textContent === value);
    items[index].click();
    await delay(DELAY);
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  registerHotkey();

})();
