# Zendesk Tommy

## Purpose

Let your crazy flipper fingers do the work.

A super-hotkey-autotron-3000 for Zendesk.\
POC for L&R.

| CTRL +  | Description                           | Transactions Issue Type   | L&R: License troubleshooting | Trial - related inquiries | L&R Product Type  | Self-Managed Subscription | SaaS Subscription            |
|---------|---------------------------------------|---------------------------|------------------------------|---------------------------|-------------------|---------------------------|------------------------------|
| S-P     | SM Premium                            |                           |                              |                           | Self-Managed      | Premium                   |                              |
| S-U     | SM Ultimate                           |                           |                              |                           | Self-Managed      | Ultimate                  |                              |
|         |                                       |                           |                              |                           |                   |                           |                              |
| C-P     | Cloud Premium                         |                           |                              |                           | GitLab.com (SaaS) |                           | Silver/SaaS Premium customer |
| C-U     | Cloud Ultimate                        |                           |                              |                           | GitLab.com (SaaS) |                           | Gold/SaaS Ultimate custome   |
|         |                                       |                           |                              |                           |                   |                           |                              |
| C-E-P   | Cloud Exemption Premium               | Cloud Licensing (SM only) | Other                        |                           | Self-Managed      | Premium                   |                              |
| C-E-U   | Cloud Exemption Ultimate              | Cloud Licensing (SM only) | Other                        |                           | Self-Managed      | Ultimate                  |                              |
|         |                                       |                           |                              |                           |                   |                           |                              |
| S-E-S-S | Subscription Extension SM Starter     | Subscription Extension    |                              |                           | Self-Managed      | Starter                   |                              |
| S-E-S-P | Subscription Extension SM Premium     | Subscription Extension    |                              |                           | Self-Managed      | Premium                   |                              |
| S-E-S-U | Subscription Extension SM Ultimate    | Subscription Extension    |                              |                           | Self-Managed      | Ultimate                  |                              |
|         |                                       |                           |                              |                           |                   |                           |                              |
| S-E-C-S | Subscription Extension Cloud Starter  | Subscription Extension    |                              |                           | GitLab.com (SaaS) |                           | Bronze customer              |
| S-E-C-P | Subscription Extension Cloud Premium  | Subscription Extension    |                              |                           | GitLab.com (SaaS) |                           | Silver/SaaS Premium customer |
| S-E-C-U | Subscription Extension Cloud Ultimate | Subscription Extension    |                              |                           | GitLab.com (SaaS) |                           | Gold/SaaS Ultimate customer  |
|         |                                       |                           |                              |                           |                   |                           |                              |
| T-E-S   | Trial Extension SM                    | Trial - related inquiries |                              | Trial extension           | Self-Managed      | Sales Assisted Trial      |                              |
| T-E-C   | Trial Extension Cloud                 | Trial - related inquiries |                              | Trial extension           | GitLab.com (SaaS) |                           | Sales Assisted Trial         |

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_tommy/script.user.js

## Changelog

- 1.4 Work on active ticket tab
- 1.3 Add Cloud + plan
- 1.2 Add SM + plan
- 1.1 Match 'Cloud exemption' with new field
- 1.0 Public release
