// ==UserScript==
// @name          Zendesk hide checkbox
// @version       1.1
// @author        Rene Verschoor
// @description   Zendesk: hide checkbox column in views
// @match         https://gitlab.zendesk.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_checkbox
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  GM_addStyle('[data-test-id="generic-table-cells-selectable"] { display: none }');
  GM_addStyle('[data-test-id="generic-table-cells-selectable"]+th+th { display: none }');
  GM_addStyle('[data-test-id="generic-table-cells-selectable"]+td+td { display: none }');
})();
