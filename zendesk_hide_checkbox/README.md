# Zendesk hide checkbox

## Purpose

Ever noticed that each ticket shown in a view has a checkbox in the first column? \
Ever used that checkbox? \
Right, so let's get rid of it to reclaim a bit of horizontal space.

There's another column which to the best of my knowledge is never used, so that one will also be obliterated.

### Before
<img src="img/before.png" height=400 />

### After
<img src="img/after.png" height=400 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js

## Changelog

- 1.1 Better selector
- 1.0 Initial release
