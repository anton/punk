// ==UserScript==
// @name          Zendesk Squeeze Region & Type
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: Squeeze Region & Type
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_squeeze_region_type
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const MINIMUM_NR_COLUMS = 4;
const SELECTOR_HEADER = '[data-test-id="generic-table"] > thead > tr > th';
const SELECTOR_CELLS = '[data-garden-id="tables.cell"]';
const MAGIC_WORDS = {
  'Preferred Region for Support': 'Region',
  'L&R Product Type': 'Type',
  'Ticket form': 'Form',
  'All Regions': 'All',
  'Americas, USA': 'AMER',
  'Asia Pacific': 'APAC',
  'Europe, Middle East, Africa': 'EMEA',
  'GitLab.com (SaaS)': 'SaaS',
  'Self-Managed': 'SM'
};

  (function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function po(mutations) {
    let ths = document.querySelectorAll(SELECTOR_HEADER);
    if (ths.length >= MINIMUM_NR_COLUMS) {
      ths.forEach(th => {
        if (th.textContent in MAGIC_WORDS)
          th.textContent = MAGIC_WORDS[th.textContent];
      })
    }
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll(SELECTOR_CELLS).forEach( element => {
        if (element.textContent in MAGIC_WORDS)
          element.textContent = MAGIC_WORDS[element.textContent];
      })
    })
  }

})();
