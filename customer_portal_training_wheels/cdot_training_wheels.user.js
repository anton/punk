// ==UserScript==
// @name         Customer Portal Training Wheels (prod)
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Augment pages in the GitLab Customer Portal
// @author       @rrelax (Tom McAtee)
// @match        https://customers.gitlab.com/admin/*
// @match        https://customers.staging.gitlab.com/admin/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @resource     https://fonts.cdnfonts.com/css/open-sans
// @require      customerpage.js
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    window.cachedURL = "";

    function handlePageChange() {
        const customerPageRegex = /\/admin\/customer\/\d*[/]?$/;
        const url = window.location.href;
        if (customerPageRegex.test(url)) {
          // The current page URL matches the regex
          console.log("Customer page detected");
            handleCustomerPage();
        } else {
          // The current page URL does not match the regex
          // console.log("Page URL does not match the regex");
        }
    }

    function mCallback (mutations) {
        if(window.location.pathname == window.cachedURL) {
            //Do nothing, no page change
        } else {
            console.log(`New page, run page processor - ${window.location.pathname}`);
            window.cachedURL = window.location.pathname
            handlePageChange();
        }
    }

    function createMutationObserver() {
        console.log("[i]\tCreating mutation observer...");
        let classQuery = "py-2 m-2 border-bottom";
         let mountCandidates = document.getElementsByClassName(classQuery);
        if(mountCandidates.length != 1) {
            console.log(`[!]\tExpected 1 result for elements with className '${classQuery}', found ${mountCandidates.length}.`);
        } else {
            let watchElement = document;
            let observerOptions = {
                subtree: true,
                childList: true,
            };
            let observer = new MutationObserver(mCallback);
            observer.observe(watchElement, observerOptions);
        }
    }

    setTimeout(() => {
        createMutationObserver();
    }, 1500);

    //Run logic on pageload
    handlePageChange();
})();