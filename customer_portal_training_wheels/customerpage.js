'use strict'

addBadgeStyles();

function addBadges() {
    console.log("Checking whether to add badges");
    //Check if billable flag activated
    let billableField = document.querySelector(".billable_field");
    if (billableField.querySelector(".bg-success") == null) {
        console.log("Adding read-only badge");
        if (document.querySelector(".readOnlyBadge") == null) {
            addAccountBadge('👀 Read-Only', '#214b6c', 'readOnlyBadge');
        }
    } else {
        console.log("Not read only");
    }

    //Check if sign-in enabled
    let signInField = document.querySelector(".login_activated_field");
    if (signInField.querySelector(".bg-success") == null) {
        console.log("Adding locked badge");
        if (document.querySelector(".lockedBadge") == null) {
            addAccountBadge("🔒 Locked", "#ddd", "lockedBadge");
        }
    } else {
        //No sign-in locked badge necessary, user can sign in
    }
    resolveUID();
}

function handleCustomerPage() {
    console.log("[i]\tCustomer page detected");
    setTimeout(() => {
        addBadges();
    }, 750);
}

function resolveUID() {
    const uidField = document.querySelector('.uid_field');

    // If no element was found, exit the function
    if (!uidField) return;

    if (document.querySelector(".saasBadge") != null) return;

    const cardBody = uidField.querySelector('.card-body');
    if (cardBody) {
        // If the innerHTML of the cardBody element contains a "-", exit the function
        if (cardBody.innerHTML.includes('-')) return;
        const userId = cardBody.innerHTML.trim();
        const requestURL = `https://gitlab.com/api/v4/users/${userId}`;
        console.log(`Requesting: ${requestURL}`);
        GM_xmlhttpRequest({
            method: 'GET',
            url: requestURL,
            onload: function (response) {
                // Parse the response text as JSON
                const json = JSON.parse(response.responseText);

                // If the JSON object contains an "error" key, print the object to the console and exit the function
                if (json.error) {
                    console.log(json);
                    return;
                }

                // If the JSON object contains a "username" key, create an anchor element
                const anchor = document.createElement('a');

                // Set the innerHTML of the anchor element to the value of the "username" key in the JSON object
                anchor.innerHTML = `GitLab Account: ${json.username}`;

                // Set the href attribute of the anchor element to the specified URL
                anchor.href = `https://gitlab.com/${json.username}`;

                // Append the anchor element to the valueCell element
                cardBody.appendChild(anchor);

                var container = document.querySelector('.list-group .accountBadgeContainer');
                if (!container) {
                    container = document.createElement('div');
                    container.className = 'accountBadgeContainer';
                    var listGroup = document.querySelector('.list-group');
                    if (listGroup) {
                        listGroup.insertBefore(container, listGroup.firstChild);
                    } else {
                        document.body.appendChild(container);
                    }
                }
                //                        let target = document.getElementById("generatedItemsMarker");
                //  container.appendChild(anchor);
                addAccountBadge(`👤 @${json.username} `, '#e28743', 'saasBadge linkCursor', anchor.href);
            },
        });
    }

}

function addBadgeStyles() {
    var badgeStyles = `
    .trainingWheelsBadge {
      display: inline-block;
      padding: 5px 10px;
      border-radius: 20px;
      font-size: 14px;
      font-weight: bold;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-family: 'Open Sans', sans-serif;
      color: #fff;
      background-color: #efefef;

      user-select: none;
    }

    .lockedBadge {
        background-color: #efefef  !important;
        border: solid 3px black  !important;
        color: black  !important;
        cursor: default;
    }

    .readOnlyBadge {
        border:solid 3px #214b6c  !important;
        background-color: #cce6ff !important;
        color: black  !important;
        cursor: default;
      }

   .billingScrollPoint {
       border: solid 3px #00cc00 !important;
       background-color:  #70db70 !important;
       color: black !important;

   }

   .linkCursor{
       cusor: pointer !important;
   }

   .saasBadge{
       background-color: #eab676 !important;
       border: solid 3px #e28743 !important;
   }
  `
    // Add CSS styles to the page
    GM_addStyle(badgeStyles);
}


// Function to generate a badge
function createBadge(text, color, className) {
    var badge = document.createElement('span');
    badge.innerHTML = text;
    badge.className = 'trainingWheelsBadge ' + className;
    return badge;
}

function addAccountBadge(text, color, className, URL) {
    var container = document.querySelector('.list-group .accountBadgeContainer');
    if (!container) {
        container = document.createElement('div');
        container.className = 'accountBadgeContainer';
        var listGroup = document.querySelector('.list-group');
        if (listGroup) {
            listGroup.insertBefore(container, listGroup.firstChild);
        } else {
            document.body.appendChild(container);
        }
    }
    var badge = createBadge(text, color, className);

    if (URL != undefined && URL != null) {
        let anchorElement = document.createElement("a");
        anchorElement.href = URL;
        anchorElement.target = "_blank";
        anchorElement.appendChild(badge);
        badge = anchorElement;
    }
    container.appendChild(badge);
}

// Wrapper function to insert badge element into a container
function insertBadge(text, color, className, containerClass) {
    var container = document.querySelector('.' + containerClass);
    if (container) {
        var badge = createBadge(text, color, className);
        container.insertBefore(badge, container.firstChild);
    }
}