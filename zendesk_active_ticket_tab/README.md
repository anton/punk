# Zendesk active ticket tab

## Purpose

A userscript for Zendesk tickets.

Zendesk gives the active ticket tab a barely distinguishable background color. \
This script improves that.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_active_ticket_tab/script.user.js

## Configuration

To override the default color, edit the `color` value in the user manager's `Values` settings:

`Key` : `color` \
`Value` : `"HotPink"`

## Changelog

- 1.0.2
  - Improved configuration
- 1.0.1
  - Fixed @match
- 1.0 
  - Public release
