# Zendesk Timezone Toggler

## Purpose

A userscript for Zendesk tickets.

Toggles the timestamps between the current agent's local time and UTC.

![demo](img/demo.gif)

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_timezone_toggler/script.user.js

## Changelog

- 1.1 Fix for Agent Workspace
- 1.0 Public release
