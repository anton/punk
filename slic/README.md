# slic

## Purpose

Replacement for the [slic browser extension](https://gitlab.com/rverschoor/slic/)

Copy information from the following pages into clipboard:

- CustomersDot 
  - Licenses (https://customers.gitlab.com/admin/license/*)
  - Customers/Edit (https://customers.gitlab.com/admin/customer/*/edit)
  - Validate License (https://customers.gitlab.com/admin/license/validate_license)
- Salesforce
  - Account
  - Opportunity
  - Subscription

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/slic/script.user.js

## Use

Hotkey: Shift-Meta-c \
The `Meta` key is the `⌘/command` key on a Mac, and the Windows key on most other keyboards.

Alternatively use the `slic` command from the userscript manager extension dropdown. 

After triggering the `slic` script through the hotkey or menu, the information from the page is available in the clipboard and ready to be pasted in your ticket, issue, mail, or document.

## Configuration

No configuration yet.

## Changelog

- 1.3 Fix CDot validate license scraping
- 1.2 Copy license type from CDot License
- 1.1.2 Return fenced code block
- 1.1.1 Match Staging CDot
- 1.1   CDot License update for new RailsAdmin version
- 1.0.9 Scrape CDot Validate License 
- 1.0.8 ignore, version increased by mistake while still working on a (failed) SFDC Quote scrape
- 1.0.7 Scrape SFDC Subscription
- 1.0.6 Clearer toast message
- 1.0.5 Scrape SFDC Opportunity
- 1.0.4 Scrape CDot Customers/Edit, removed LicenseDot scraping
- 1.0.3 Replace LUC/PUC/TUC in CustomersDot template with full descriptions
- 1.0.2 Replace LUC/PUC/TUC in LicenseDot template with full descriptions
- 1.0.1 Better URL matching
- 1.0.0 Initial release
