'use strict';

function scrapeSfdcAccount(url) {
  let info = {};
  info.url = url;
  let labelNodes = document.querySelectorAll('.detailList tr td.labelCol');
  info.account_name = labelToValue('Account Name', labelNodes).replace('[View Hierarchy]', '').trim();
  info.account_owner = labelToValue('Account Owner (Text)', labelNodes);
  return pasteSfdcAccount(info);
}

function pasteSfdcAccount(info) {
  let text =
    `SFDC Account information:\n` +
    `URL = ${info.url}\n` +
    `Account Name = ${info.account_name}\n` +
    `Account Owner = ${info.account_owner}\n`
  ;
  return text;
}
