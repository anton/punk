'use strict';

function scrapeCDotValidateLicense(url) {
  let info = {};

  info.result = document.querySelector('div.alert.alert-success').firstChild.textContent.trim();

  let dt = document.querySelectorAll('div.alert.alert-success dl dt');
  let dd = document.querySelectorAll('div.alert.alert-success dl dd');
  if (dt.length && (dt.length == dd.length)) {
    for (let i = 0; i < dt.length; i++) {
      let key = dt[i].textContent.trim();
      let value = dt[i].nextElementSibling.textContent.trim();
      info[key] = value;
    }
  } else {
    toast_error('dt and dd length not equal');
  }
  return pasteCDotValidateLicense(info);
}

function _expandHashString(hashString) {
  let text = '';
  hashString.slice(1, -1).split(',').forEach(function(values) {
    text += `  ${values.trim()}\n`;
  })
  return text;
}

function pasteCDotValidateLicense(info) {
  let text =
    `${info.result}\n` +
    `version = ${info.version}\n` +
    `licensee =\n` +
    _expandHashString(info.licensee) +
    `issued_at = ${info.issued_at}\n` +
    `expires_at = ${info.expires_at}\n` +
    `notify_admins_at = ${info.notify_admins_at}\n` +
    `notify_users_at = ${info.notify_users_at}\n` +
    `block_changes_at = ${info.block_changes_at}\n` +
    `cloud_licensing_enabled = ${info.cloud_licensing_enabled}\n` +
    `offline_cloud_licensing_enabled = ${info.offline_cloud_licensing_enabled}\n` +
    `auto_renew_enabled = ${info.auto_renew_enabled}\n` +
    `seat_reconciliation_enabled = ${info.seat_reconciliation_enabled}\n` +
    `operational_metrics_enabled = ${info.operational_metrics_enabled}\n` +
    `generated_from_customers_dot = ${info.generated_from_customers_dot}\n` +
    `restrictions =\n` +
    _expandHashString(info.restrictions)
  ;
  return text;
}

