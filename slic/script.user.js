// ==UserScript==
// @name          slic
// @version       1.3
// @author        Rene Verschoor
// @description   slic
// @match         https://customers.gitlab.com/admin/*
// @match         https://customers.staging.gitlab.com/admin/*
// @match         https://gitlab.my.salesforce.com/*
// @grant         GM_registerMenuCommand
// @grant         GM_setClipboard
// @grant         GM_log
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @require       message.js
// @require       cdot_license.js
// @require       cdot_customer_edit.js
// @require       cdot_validate_license.js
// @require       sfdc.js
// @require       sfdc_opportunity.js
// @require       sfdc_subscription.js
// @require       sfdc_account.js
// @noframes
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/slic
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/slic/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/slic/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/slic/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

// https://github.com/violentmonkey/vm-shortcut
const HOTKEY = 'shift-meta-c';

(function () {

  function registerHotkey() {
    VM.shortcut.register(HOTKEY, () => {
      pogo();
    });
  }

  function registerMenu() {
    GM_registerMenuCommand('slic', pogo);
  }

  function paste(text) {
    if (text.trim().length > 0) {
      GM_setClipboard(codeFence(text), 'text/plain');
      toast_ok('Information is copied');
    } else {
      toast_error('Nothing copied');
    }
  }

  function codeFence(text) {
    return "```\n" + text + "```\n"
  }

  function pogo() {
    let url = document.URL;
    if (url.match(/^https:\/\/customers\.(staging\.)?gitlab.com\/admin\/license\/validate_license/)) {
      paste(scrapeCDotValidateLicense(url));
    } else if (url.match(/^https:\/\/customers\.(staging\.)?gitlab.com\/admin\/license\/\d+/)) {
      paste(scrapeCDotLicense(url));
    } else if (url.match(/^https:\/\/customers\.(staging\.)?gitlab.com\/admin\/customer\/\d+\/edit/)) {
      paste(scrapeCDotCustomerEdit(url));
    } else if (url.match(/^https:\/\/gitlab\.my\.salesforce.com/)) {
      paste(scrapeSfdc(url));
    }
  }

  function slic() {
    registerMenu();
    registerHotkey();
  }

  slic();

})();
