# Zendesk unsupported version

## Purpose

A userscript for Zendesk tickets.

This script alerts you when the customer seems to be on an unsupported GitLab version.

<img src="img/screenshot.png" height=300 />

Clicking on the `GitLab Version` label text will lead you to the `Version Support` [text in the handbook](https://about.gitlab.com/support/statement-of-support/#version-support).

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js

## Configuration

There is no configuration.\
The list of supported versions is hardcoded in the source code. \
Once a year, when GitLab jumps to a new major version, the userscript needs to be adapted. \
Everyone who has the userscript installed will automatically receive the updated version.

## Changelog

- 1.2 Link `GitLab Version` label to handbook
- 1.1 GL 16 release
- 1.0 Public release
