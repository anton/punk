// ==UserScript==
// @name          Zendesk unsupported version
// @version       1.2
// @author        Rene Verschoor
// @description   Zendesk: unsupported GitLab version warning
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_unsupported_version
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const SUPPORTED_VERSIONS = ['14', '15', '16'];
const HANDBOOK_URL = 'https://about.gitlab.com/support/statement-of-support/#version-support';

(function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    pogo();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function pogo() {
    let ticket= getActiveTicket();
    if (ticket) {
      let selector = '.custom_field_43970347 input';
      let elements = ticket.querySelectorAll(selector);
      elements.forEach((element, index) => {
        if (isUnsupported(element.value)) {
          highlightVersionField(element);
          linkifyVersionField(element);
        }
      })
    }
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  function isUnsupported(version) {
    let major = version.split('.')[0];
    return major.length && !SUPPORTED_VERSIONS.includes(major);
  }

  function highlightVersionField(field) {
    field.style.boxShadow='0px 0px 10px 5px red';
  }

  function linkifyVersionField(field) {
    const punked = field.getAttribute('punked');
    if (!punked) {
      field.setAttribute('punked', 'true');
      const labelElement = field.previousElementSibling;
      const newElement = Object.assign(document.createElement('a'), {
        href: HANDBOOK_URL,
        target: '_blank',
        textContent: labelElement.textContent
      });
      labelElement.textContent = '';
      labelElement.appendChild(newElement);
    }
  }

})();
