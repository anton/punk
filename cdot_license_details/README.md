# CDot License Details

## Purpose

Show a condensed view of the CDot License page.\
Information is now visible at a glance.

The original information is still available, just clcik the `Show original info` button.

<img src="img/screen.png" height=400 />

## Extra information

Both license type and trial indication are shown on the original page.\
Instead of searching for this information, I make them clearly visible.

A few validity checks are also run, and will alert you e.g. when a license is expired. 

<img src="img/analysis.png" height=100 />

## Email search

The email address of the license contact is shown in four places on the screen, and formatted a bit weird. \
Each shown email address is in fact a search action:

- `Email` - search in CDot Licenses
- `CDot Licenses` - same as `Email`, it will search in CDot Licenses. 
- `CDot Customers` - search in CDot Customer
- `SFDC` - search Salesforce

Email addresses are shown as `username`@`domain.abc`, where both the `username` and `domain.abc` parts are clickable links.\
When you click on the `username` part, the search is done with the normal complete `username@domain.abc` address.\
When you click on the `domain.abc` part, the search is done with just `domain.abc`.

The `❐` icon next to teh `Email` address copies the emaill address into your clipboard.

<img src="img/email.png" height=400 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js

## Changelog

- 1.5 Add CDot Customers + SFDC search links
- 1.4 Add email copy icon
- 1.3 Avoid duplicate DOM inserts after history traversal
- 1.2 Expired indication
- 1.1 Only show trials in analysis
- 1.0 Public release
