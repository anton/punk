// ==UserScript==
// @name          cdot_license_details
// @version       1.5
// @author        Rene Verschoor
// @description   CDot License details
// @match         https://customers.gitlab.com/*
// @match         https://customers.staging.gitlab.com/*
// @grant         GM_setClipboard
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/cdot_license_details
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  // Watch for a page URL change
  let previousUrl = '';
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  let observer = new MutationObserver(function (mutations, observer) {
    let url = location.href;
    if (url !== previousUrl) {
      previousUrl = url;
      po(url);
    }
  });
  observer.observe(document, { childList: true, subtree: true });

  function po(url) {
    // Only act when we're looking at a license details page
    if (url.match(/^https:\/\/customers\.(staging\.)?gitlab.com\/admin\/license\/\d+$/)) {
      go();
    }
  }

  async function go() {
    // Collect license info
    const info = await scrape();
    // Display license info My Way
    await myWay(info);
  }

  async function scrape() {
    let info = {};
    await delay(DELAY);
    hideOriginalInfo();
    const headers = document.querySelectorAll('.card-header');
    if (headers.length) {
      for (let i = 0; i < headers.length; i++) {
        // Field name, eg '  First name  \n' -> remove whitespace and LF/CR, lowercase, snake_case -> 'first_name'
        const key = headers[i].textContent.trim().toLowerCase().replace(/ /g, '_');
        const value = headers[i].nextElementSibling.textContent.trim();
        info[key] = value;
      }
    }
    return info;
  }

  const DELAY = 200;

  function delay(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  const css =
    `
      <style>
        #punk {
          display: flex;
          flex-wrap: wrap;
          align-items: start;
          margin-bottom: 1rem;
        }
        table {
          margin: 0.5rem;
        }
        td.label {
          text-align: right;
          padding-left: 0.5rem;
          padding-right: 0.8rem;
          background-color: rgb(236, 236, 239);
          border-left: solid white 2px;
        }
        td.value {
          padding-left: 0.2rem;
          padding-right: 0.5rem;
        }
        #toggleButton {
          padding: 0.5rem;
          margin-bottom: 1rem;
          border: 0;
        }
        #copyEmail {
          cursor: pointer; 
        }
      </style>
      `

  async function myWay(info) {
    await delay(DELAY);
    const punked = await alreadyPunked();
    if (!punked) {
      await addInfoToPage(info);
      await delay(DELAY);
    }
    addToggleButtonListener();
    addEmailCopyListener(info);
  }

  async function alreadyPunked() {
    return document.querySelector('div#punk');
  }

  async function addInfoToPage(info) {
    let html = '';
    html += '<div id="punk">'
    html += addInfoTable(info);
    html += addAnalysis(info);
    html += '</div>'
    html += addToggleButton();
    addToDOM(html);
  }

  function addToDOM(html) {
    const div = document.createElement('div')
    div.innerHTML += css;
    div.innerHTML += html;

    const target = document.querySelector('div.fieldset')
    target.appendChild(div)
    target.before(div)
  }

  function addToggleButton() {
    return '<button id="toggleButton">Show original info</button>'
  }

  function addInfoTable(info) {
    let html = '';
    infoTable(info).forEach((section) => {
      html += '<table>'
      section.forEach((line) => {
        html += tableRow(line.label, showValue(line.label, line.value));
      });
      html += '</table>'
    });
  return html;
  }

  function tableRow(label, value) {
    let html = '';
    html += '<tr>';
    html += `<td class="label">${label}</td>`
    html += `<td class="value">${value}</td>`
    html += '</tr>';
    return html;
  }

  function infoTable(info) {
    return [
      [ {label: 'Company', value: info.company},
        {label: 'Name', value: info.name},
        {label: 'Email', value: info.email},
        {label: '&nbsp;', value: ''},
        {label: 'Type', value: info.type},
        {label: 'Plan', value: info.gitlab_plan},
        {label: 'Trial', value: info.trial},
        {label: '&nbsp;', value: ''},
        {label: 'Users count', value: info.users_count},
        {label: 'Previous users', value: info.previous_users_count},
        {label: 'Trueup count', value: info.trueup_count} ],
      [ {label: 'Issued at', value: info.issued_at},
        {label: 'Starts at', value: info.starts_at},
        {label: 'Expires at', value: info.expires_at},
        {label: '&nbsp;', value: ''},
        {label: 'Zuora id', value: info.zuora_subscription},
        {label: 'Zuora name', value: info.zuora_subscription_name},
        {label: '&nbsp;', value: ''},
        {label: 'Creator', value: info.creator},
        {label: 'Notes', value: info.notes}
      ]
    ];
  }

  function showValue(label, value) {
    let text;
    // Some values still have a trailing space...
    value.trim();
    switch (label) {
      case 'Company': {
        text = showCompany(value);
        break;
      }
      case 'Name': {
        text = showName(value);
        break;
      }
      case 'Email': {
        text = showEmail(value);
        break;
      }
      case 'Zuora id': {
        text = showZuoraId(value);
        break;
      }
      default: {
        text = value;
      }
    }
    return text;
  }

  function showCompany(value) {
    return linkifyCDotLicenses(value);
  }

  function showName(value) {
    return linkifyCDotLicenses(value);
  }

  function showEmail(value) {
    return linkifyCDotLicensesEmail(value) + showEmailCopy();
  }

  function showEmailCopy() {
    return '&nbsp;<i id="copyEmail">❐</i>';
  }

  function showZuoraId(value) {
    if (value === 'N/A') {
      return value;
    } else {
      return linkifyZuora(value);
    }
  }

  function linkifyCDotLicenses(value) {
    return linkifyCDot('license', value);
  }

  function linkifyCDotLicensesEmail(email) {
    return linkifyCDotEmail('license', email);
  }

  function linkifyCDotCustomersEmail(email) {
    return linkifyCDotEmail('customer', email);
  }

  function linkifyCDotEmail(path, email) {
    const parts = splitEmail(email);
    return linkifyCDot(path, email, parts.name) + '@' + linkifyCDot(path, parts.domain, parts.domain);
  }

  function linkifyCDot(path, value, text = value) {
    const url = new URL(`https://customers.gitlab.com/admin/${path}`);
    url.searchParams.append('query', `${value}`);
    return `<a href="${url}">${text}</a>`;
  }

  function linkifyZuora(value, text = value) {
    const url = new URL('https://www.zuora.com/apps/Subscription.do');
    url.searchParams.append('method', 'view');
    url.searchParams.append('id', `${value}`);
    return `<a href="${url}">${text}</a>`;
  }

  function splitEmail(email) {
    const parts = email.split('@');
    return { name: parts[0], domain: parts[1]};
  }

  function linkifySFDCEmail(email) {
    const parts = splitEmail(email);
    return linkifySFDC(email, parts.name) + '@' + linkifySFDC(parts.domain, parts.domain);
  }

  function linkifySFDC(value, text = value) {
    const url = new URL('https://gitlab.my.salesforce.com/_ui/search/ui/UnifiedSearchResults');
    url.searchParams.append('str', `${value}`);
    return `<a href="${url}">${text}</a>`;
  }

  function addToggleButtonListener() {
    const button = document.getElementById ("toggleButton");
    button.addEventListener ("click", toggleOriginalInfo, false);
  }

  function toggleOriginalInfo() {
    if (this.textContent.startsWith('Show')) {
      this.textContent = 'Hide original info';
      showOriginalInfo();
    } else {
      this.textContent = 'Show original info';
      hideOriginalInfo();
    }
  }

  function showOriginalInfo() {
    const originalInfo = document.getElementsByClassName("fieldset");
    originalInfo[0].style.visibility = 'visible';
  }

  function hideOriginalInfo() {
    const originalInfo = document.getElementsByClassName("fieldset");
    originalInfo[0].style.visibility = 'hidden';
  }

  function addEmailCopyListener(info) {
    const icon = document.getElementById ("copyEmail");
    icon.addEventListener ("click", () => { copyEmail(info) }, false);
  }

  function copyEmail(info) {
    GM_setClipboard(info.email, 'text/plain');
    toast_ok('Email copied');
  }

  function addAnalysis(info) {
    let html = '';
    html += '<table>'
    html += analysisTrial(info);
    html += analysisType(info);
    html += analysisDuration(info);
    html += analysisExpiration(info);
    html += addLinks(info);
    html += '</table>'
    return html;
  }

  function analysisTrial(info) {
    let html = '';
    if (isTrial(info)) {
      html += tableRow('🟢', 'Trial license');
    }
    return html;
  }

  function analysisType(info) {
    const types = { 'Legacy': '⌛', 'Offline': '🔒', 'Cloud': '☁️' };
    return tableRow(types[info.type], info.type);
  }

  function analysisDuration(info) {
    let html = '';
    const trial = isTrial(info);
    let oneyear = exactlyOneYear(info.starts_at, info.expires_at);
    let yearplus = oneYearOrMore(info.starts_at, info.expires_at);
    if (!trial && !oneyear) {
      html = tableRow('📅', `License != 1y ❗`);
    }
    if (trial && yearplus) {
      html = tableRow('📅', `Trial >= 1y ❗`);
    }
    return html;
  }

  function analysisExpiration(info) {
    let html = '';
    const today = new Date();
    const expiration = new Date(info.expires_at);
    if (expiration <= today) {
      html = tableRow('⏰', `Expired ❗`);
    }
    return html;
  }

  function exactlyOneYear(start, end) {
    const startYMD = start.split('-');
    const endYMD = end.split('-');
    return ( (endYMD[0] - startYMD[0] === 1) && (startYMD[1] === endYMD[1]) && (startYMD[2] === endYMD[2]) );
  }

  function oneYearOrMore(start, end) {
    const startYMD = start.split('-');
    const endYMD = end.split('-');
    return (endYMD[0] - startYMD[0] == 1);
  }

  function isTrial(info) {
    return (info.trial === 'Yes');
  }

  // https://github.com/sweetalert2/sweetalert2
  function _toast(icon, message) {
    let timerInterval
    Swal.fire({
      icon: icon,
      title: message,
      showConfirmButton: false,
      timer: 1500,
      willClose: () => {clearInterval(timerInterval)},
    });
  }

  function toast_ok(message) {
    _toast('success', message);
  }

  function addLinks(info) {
    let html = '';
    html += tableRow('&nbsp;', '');
    html += tableRow('CDot Licenses', linkifyCDotLicensesEmail(info.email));
    html += tableRow('CDot Customers', linkifyCDotCustomersEmail(info.email));
    html += tableRow('SFDC', linkifySFDCEmail(info.email));
    return html;
  }
  })();
